/**
 * 
 */
package postgreSQLDatabase.feePayment;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import settings.database.*;
import users.Student;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.postgresql.util.PGobject;

import exceptions.IncorrectFormatException;

/**
 * @author Shubhi Anita
 *
 */
public class Query {

	private static PreparedStatement proc;

	public static void main(String[] args) throws SQLException, IncorrectFormatException {
	//	getFeeTransactionById(4L);
		// ArrayList<Payment> fee=getFeePaymentHistory(1);
		// getFeeBreakup(1);
		// System.out.println(getFeePaymentDetails(69));
		// getFeeJson(69);
		 getTransactionId(2016, 6, 225);
		
		
		// getFeeBreakup(2, 2016);
		// getFeePaymentByTransaction(4L);
	//	isVerifyFeeTransactions(52L);
	//	verifyFeeTransaction(5L);
		// isVerifyFeePayment(4L);
		// System.out.println("hi"+getFeePaymentBySemRegId(1,200 ));

		// deletePayment(59);
		
		getFeeTransactionById(4l);
	}

	public static ArrayList<Student> getStudentsListByTransaction() throws SQLException, IncorrectFormatException {
		ArrayList<Student> student_list = null;
		Student current;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getStudentsListByTransacation\"();");
			student_list = new ArrayList<Student>();
			ResultSet rs = proc.executeQuery();
			while (rs.next()) {
				current = new Student(rs);
				student_list.add(current);
			}

			rs.close();
			proc.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return student_list;
	}
	public static ArrayList<Student> getStudentsListByRegistrationTransaction() throws SQLException, IncorrectFormatException {
		ArrayList<Student> student_list = null;
		Student current;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getStudentsListByRegistrationTransaction\"();");
			student_list = new ArrayList<Student>();
			ResultSet rs = proc.executeQuery();
			while (rs.next()) {
				current = new Student(rs);
				student_list.add(current);
			}

			rs.close();
			proc.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return student_list;
	}

	public static ArrayList<Payment> getFeePaymentHistory(long user_id) {

		ArrayList<Payment> history_info = new ArrayList<Payment>();
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeePaymentHistory\"(?);");

			proc.setObject(1, user_id);
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);

			while (rs.next()) {

				Payment history = new Payment();
				history.setRef_no(rs.getLong("ref_no"));
				history.setComment(rs.getString("comment"));
				history.setDetails(new JSONObject(rs.getString("details")));
				history.setAmount(rs.getLong("amount"));
				history.setVerified(rs.getBoolean("verified"));
				history.setPayment_method(rs.getInt("payment_method"));
				history_info.add(history);
			}
			rs.close();
			proc.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return history_info;
	}

	public static ArrayList<FeeBreakup> getFeePaymentList() {

		ArrayList<FeeBreakup> payment_list = new ArrayList<FeeBreakup>();
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeePaymentList\"();");

			ResultSet rs = proc.executeQuery();
			System.out.println(proc);

			while (rs.next()) {

				FeeBreakup breakup = new FeeBreakup();
				breakup.setCategory(rs.getString("category"));
				breakup.setTotal_amt(rs.getInt("total_amt"));
				breakup.setSemester(rs.getInt("semester"));
				breakup.setYear(rs.getInt("year"));
				JSONArray j_array = new JSONArray();
				Array breakup_array = rs.getArray("breakup");
				// System.out.println(breakup_array);
				ResultSet as = breakup_array.getResultSet();
				while (as.next()) {
					j_array.put(new JSONObject(as.getString(2)));
				}
				breakup.setBreakup(j_array);
				payment_list.add(breakup);
			}
			//System.out.println();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return payment_list;

	}

	public static FeePaymentDetails getFeePaymentDetails(long user_id) {
		FeePaymentDetails payment_details = new FeePaymentDetails();
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeePaymentDetails\"(?);");

			proc.setObject(1, user_id);
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			rs.next();

			payment_details.setId(rs.getInt("ref_no"));
			payment_details.setName(rs.getString("name"));
			JSONObject details = new JSONObject(rs.getString("details"));
			payment_details.setDetails(details);
			payment_details.setPayment_method(rs.getInt("payment_method"));
			rs.close();
			proc.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return payment_details;
	}

	public static void addFeeBreakup(int semester, String category, String breakup, int year) {
		try {

			JSONArray fee_breakup = new JSONArray(breakup);
			JSONObject amt_obj = fee_breakup.getJSONObject(fee_breakup.length() - 1);
			JSONObject j_array[] = new JSONObject[fee_breakup.length()];
			for (int i = 0; i < fee_breakup.length(); i++) {
				j_array[i] = fee_breakup.getJSONObject(i);

			}
			int amount = amt_obj.getInt("total");
			// System.out.println(amount);
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addFeeBreakup\"(?,?,?,?,?);");
			proc.setInt(1, year);
			proc.setInt(2, semester);
			proc.setString(3, category);
			proc.setObject(4, PostgreSQLConnection.getConnection().createArrayOf("json", j_array));
			proc.setInt(5, amount);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static JSONArray getFeeJson(Long reg_id) {

		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getFeeJson\"(?);");
			proc.setLong(1, reg_id);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			JSONArray j_array = new JSONArray();
			ResultSet as = rs.getArray(1).getResultSet();
			while (as.next()) {
				JSONObject current = new JSONObject(as.getString(2));
				j_array.put(current);
			}
			return j_array;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static ArrayList<FeeBreakup> getFeeBreakup(int semester, int year) {

		ArrayList<FeeBreakup> getList = new ArrayList<FeeBreakup>();
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeeBreakup\"(?,?);");
			proc.setInt(1, semester);
			proc.setInt(2, year);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();

			while (rs.next()) {
				FeeBreakup feebreakup = new FeeBreakup();
				feebreakup.setCategory(rs.getString("category"));
				feebreakup.setSemester(rs.getInt("semester"));
				feebreakup.setYear(rs.getInt("year"));
				feebreakup.setTotal_amt(rs.getInt("total_amt"));
				JSONArray breakup = new JSONArray();
				ResultSet as = rs.getArray("breakup").getResultSet();
				while (as.next()) {
					JSONObject json_object = new JSONObject(as.getString(2));
					breakup.put(json_object);
				}
				feebreakup.setBreakup(breakup);
				getList.add(feebreakup);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getList;
	}
	// insert into payment table

	/**
	 * @param comment
	 * @param pay_method
	 * @param details
	 * @param amt
	 * @param reg_id
	 * @param payee
	 * @param beneficiary
	 * @param semester
	 * @return
	 */
	public static Long addFeePayment(String comment, int pay_method, JSONObject details, long amt, long payee,
			long beneficiary, long transaction_id) {

		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addFeePayment\"(?,?,?,?,?,?,?);");

			proc.setString(1, comment);
			proc.setInt(2, pay_method);
			PGobject jsonObject = new PGobject();
			jsonObject.setType("json");
			jsonObject.setValue(details.toString());

			proc.setObject(3, jsonObject);

			proc.setLong(4, amt);
			proc.setLong(5, payee);
			proc.setLong(6, beneficiary);
			proc.setLong(7, transaction_id);
			// System.out.println(proc);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			// System.out.println(rs.getInt(1));

			return rs.getLong(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0L;

	}

	public static Long addFeeTransaction(int semester, String category, long reg_id, int year) {

		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addFeeTransaction\"(?,?,?,?);");

			proc.setInt(1, semester);

			proc.setString(2, category);

			proc.setLong(3, reg_id);
			proc.setInt(4, year);
			ResultSet rs=proc.executeQuery();
			rs.next();
			return rs.getLong(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
              return 0L;
	}

	public static long verifyFeePayment(long ref_no) {
		ResultSet rs;
		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"feeVerify\"(?);");
			proc.setLong(1, ref_no);
			 rs = proc.executeQuery();
			 rs.next();
             return rs.getLong(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public static long retrieveFeeAmount(Long reg_id) {

		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"getFeeAmount\"(?);");

			proc.setLong(1, reg_id);
			// System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			// System.out.println(rs.getLong(1));
			return rs.getLong(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public static long getTransactionId(int year, int sem, long reg_id) throws SQLException, IncorrectFormatException {
		long tid = 0L;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"getTransactionId\"(?,?,?);");
			proc.setInt(1, year);
			proc.setInt(2, sem);
			proc.setLong(3, reg_id);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			tid = rs.getLong(1);
			return tid;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  return tid;
	}

	public static JSONObject getFeeJson(int reg_id) throws SQLException {
		PreparedStatement proc = PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT public.\"getFeeJson\"(?);");
		try {
			proc.setObject(1, reg_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			String fee = rs.getString(1);
			JSONObject fee_breakup = new JSONObject(fee);
			return fee_breakup;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static ArrayList<Transaction> getFeeTransactions(Long reg_id) {
		ArrayList<Transaction> transcation_list = null;
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeeTransactions\"(?);");
			proc.setObject(1, reg_id);
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);

			transcation_list = new ArrayList<Transaction>();
			while (rs.next()) {
				Transaction current = new Transaction();
		current.setCategory(rs.getString("category"));
		current.setCompleted(rs.getBoolean("completed"));
		current.setDate(rs.getDate("date"));
		current.setSemester(rs.getInt("semester"));
		current.setYear(rs.getInt("year"));
		current.setTransaction_id(rs.getLong("transaction_id"));
				// System.out.println(rs.getLong("transaction_id"));
			transcation_list.add(current);
			}
		} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return (transcation_list);

	}

	public static ArrayList<Payment> getFeePaymentByTransaction(Long transaction_id) {
		ArrayList<Payment> payment_list = null;

		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeePaymentByTransaction\"(?);");
			proc.setObject(1, transaction_id);
			ResultSet rs = proc.executeQuery();
			// System.out.println(proc);
			payment_list = new ArrayList<Payment>();
			while (rs.next()) {
				Payment current = new Payment();
				current.setAmount(rs.getLong("amount"));
				current.setComment(rs.getString("comment"));
				current.setPayment_method(rs.getInt("payment_method"));
				current.setVerified(rs.getBoolean("verified"));
				JSONObject j_object = new JSONObject(rs.getString("details"));
				 current.setDetails(j_object);
		        current.setRef_no(rs.getLong("ref_no"));

		payment_list.add(current);
					}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
				}

		return payment_list;

	}

	public static ArrayList<Payment> getFeePaymentBySemRegId(int sem, long reg_id) {
		ArrayList<Payment> payment_list = null;

		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeePaymentBySem&Regid\"(?,?);");
			proc.setInt(1, sem);
			proc.setLong(2, reg_id);
			ResultSet rs = proc.executeQuery();
			payment_list = new ArrayList<Payment>();
			while (rs.next()) {
				Payment current = new Payment();
				current.setAmount(rs.getLong("amount"));
				current.setComment(rs.getString("comment"));
				current.setPayment_method(rs.getInt("payment_method"));
				current.setVerified(rs.getBoolean("verified"));
				JSONObject j_object = new JSONObject(rs.getString("details"));
				current.setDetails(j_object);
				current.setRef_no(rs.getLong("ref_no"));

				payment_list.add(current);
			}

		} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
	}

		return payment_list;

	}

	public static boolean isVerifyFeeTransactions(long ref_no) {
		boolean completed = false;
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"isVerifyFeeTransactions\"(?);");
			proc.setLong(1, ref_no);
			ResultSet rs = proc.executeQuery();
			rs.next();
			completed = rs.getBoolean(1);
			return completed;
			// System.out.println(completed);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return completed;
	}

	public static void verifyFeeTransaction(long transaction_id) {

		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"verifyFeeTransaction\"(?);");
			proc.setLong(1, transaction_id);
			proc.executeQuery();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void deletePayment(long ref_no) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"deletePayment\"(?);");
			proc.setLong(1, ref_no);

			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static boolean isVerifyFeeTransaction(long ref_no) {
		boolean verified = false;
				try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"isVerifyFeeTransaction\"(?);");
					proc.setLong(1, ref_no);
					ResultSet rs = proc.executeQuery();
					rs.next();
			verified = rs.getBoolean(1);
					return verified;

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		return verified;
			}

	public static boolean isVerifyFeePayments(long transaction_id) {
		boolean verified = false;
				try {
					proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"isVerifyFeePayments\"(?);");
					proc.setLong(1, transaction_id);
					ResultSet rs = proc.executeQuery();
					rs.next();
			verified = rs.getBoolean(1);
					return verified;

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		return verified;
			}
	
	
	
	public static Payment getFeePaymentByRefNo(long ref_no) throws SQLException{
		Payment current=null;
		
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeePaymentByRefNo\"(?);");
			proc.setLong(1, ref_no);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
		current=	new Payment();
		current.setAmount(rs.getLong("amount"));
		current.setComment(rs.getString("comment"));
		JSONObject details=new JSONObject(rs.getString("details"));
		current.setDetails(details);
		current.setPayment_method(rs.getInt("payment_method"));
		current.setVerified(rs.getBoolean("verified"));
		current.setRef_no(rs.getLong("ref_no"));
		current.setTransaction_id(rs.getLong("transaction_id"));
			rs.close();
			proc.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return current;
	}
	

  
//public static Transaction getFeeTransactionById(long transaction_id) throws SQLException, IncorrectFormatException {
//        long tid = 0l;
//        Transaction transaction=null;
//        try {
//            PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
//                    .prepareStatement("SELECT * from public.\"getFeeTransactionByTransactionId\"(?);");
//            proc.setLong(1, transaction_id);
//
//            ResultSet rs = proc.executeQuery();
//            rs.next();
//         transaction=new Transaction();
//            ArrayList<Long> ref_no=new ArrayList<Long>();
//            transaction.setCategory(rs.getString("category"));
//            ResultSet as=rs.getArray("ref_no").getResultSet();
//            while(as.next()){
//                ref_no.add(Long.parseLong(as.getString(2)));
//            }
//            transaction.setRef_no(ref_no);
//            transaction.setReg_id(rs.getLong("reg_id"));
//            transaction.setSemester(rs.getInt("semester"));
//            transaction.setDate(utilities.StringFormatter.convert(rs.getDate("date")));
//            transaction.setCompleted(rs.getBoolean("category"));
//            transaction.setYear(rs.getInt("year"));
//            rs.close();
//            proc.close();
//            return transaction;
//        } catch (JSONException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//            // } catch (ParseException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//        return transaction;
//    }
//	
	
	
	

	/**
	 * @param string
	 * @param i
	 * @param details
	 * @param parseInt
	 * @param parseLong
	 * @param attribute
	 * @return
	 */
	
	public static Transaction getFeeTransactionById(long transaction_id){
		Transaction transaction=null;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFeeTransactionByTransactionId\"(?);");
			proc.setLong(1, transaction_id);
System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			System.out.println(proc);
		 transaction=new Transaction();
			ArrayList<Long> ref_no=new ArrayList<Long>();
			transaction.setCategory(rs.getString("category"));
			ResultSet as=rs.getArray("ref_no").getResultSet();
			while(as.next()){
				ref_no.add(Long.parseLong(as.getString(2)));
			}
			transaction.setTransaction_id(transaction_id);
			transaction.setRef_no(ref_no);
			transaction.setReg_id(rs.getLong("reg_id"));
			transaction.setSemester(rs.getInt("semester"));
			transaction.setDate(utilities.StringFormatter.convert(rs.getDate("date")));
			transaction.setCompleted(rs.getBoolean("completed"));
			transaction.setYear(rs.getInt("year"));
			rs.close();
			proc.close();
			return transaction;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return transaction;
	}


}
