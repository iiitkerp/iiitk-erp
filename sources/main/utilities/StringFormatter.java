/**
 * 
 */
package utilities;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Joey
 *
 */
public class StringFormatter {
public static String TitleCase(String text){
	 StringBuilder titleCase = new StringBuilder();
	    boolean nextTitleCase = true;
	 
	    for (char c : text.toCharArray()) {
	        if (Character.isSpaceChar(c)) {
	            nextTitleCase = true;
	        } else if (nextTitleCase) {
	            c = Character.toTitleCase(c);
	            nextTitleCase = false;
	        } 
	 
	        titleCase.append(c);
	    } 
	 
	    return titleCase.toString();
}
public static java.sql.Date convert(java.util.Date date){
	if(date==null)return null;
	return new java.sql.Date(date.getTime());
}
public static boolean isInt(String integer){
	try{
	Integer.parseInt(integer);
	}
	catch (Exception e){
		
		return false;
	}
	return true;
}
public static String getDisplayString(Object obj){
	if (obj instanceof Date)
	{
	System.out.println("reached");
		Date date=(Date)obj;
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.format(date);
		
	}
	if (obj instanceof java.sql.Date)
	{
	
		java.sql.Date date=(java.sql.Date)obj;
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.format(utilities.StringFormatter.convert(date));
		
	}
	
       if(obj==null)return "";
	String text=obj.toString().toUpperCase();
	if(text.equals("")||text.equals("undefined")||text.equals("null"))return "";
	else
	return text;
}



public static String booleanToString(Boolean var)	{
	if (var==true){
		return "Yes";
	}
	else if (var==false){
		return "No";
		
	}
	else if(var==null) {
		return "";
	}
	
return "";
	
}
public static String getOddEven(int semester)
{
	if(semester%2==0){
		return "Even";
	}
	else return"Odd";
}













}
