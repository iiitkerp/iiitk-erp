<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="postgreSQLDatabase.registration.Query"%>
    <%@page import="users.Student"%>
    <%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" >
<style type="text/css">
@page{
size:A4;
}
table, tr, td, th {border:1px solid black; border-collapse:collapse;}
</style>
<title>IIITK | ERP</title>
</head>
<body>
<header align="center"><h2>Student Reporting status</h2></header>
<div class="box-body" style="font-size:80%">
								<table id="example1" class="table table-bordered table-striped" >
									<thead style="font-size:120%">
										<tr>
											<th>S.N.</th>
											<th>Name</th>
											<th>Student ID</th>						
											<th>State</th>
											<th>Phone Number</th>
											<th>Email</th>
											<th>Program Allocated</th>
											<th>Category</th>
											<th>Status</th>
										
											</tr>
									</thead>
									<tbody>
										<%
                	ArrayList<Student> csab_list=postgreSQLDatabase.registration.Query.getStudentRegistrationList();
                                Iterator<Student> iterator=csab_list.iterator();
                                int index=1;
                                while(iterator.hasNext()){
                    				Student current=iterator.next();
                    				if(current.getSemester()!=1)continue;
                    				if(request.getParameter("reported")!=null&&request.getParameter("reported").equals("true") &&!current.isReported())continue;
                    				if(request.getParameter("reported")!=null&&request.getParameter("reported").equals("false") &&current.isReported())continue;
                    					
                %>
										<tr>
											<td><center><%=index++ %></center></td>
											<td><center><%=current.getName() %></center></td>
											<td><center><%=current.getStudent_id()%></center></td>
											<td><center><%=current.getState_eligibility()%></center></td>
											<td><center><%=current.getMobile()%></center></td>
											<td><center><%=current.getEmail() %></center></td>
											<td><center><%if(current.getProgram_allocated().contains("ELECTRONICS")) out.print("ECE");
											else out.print("CSE");%></center></td>
											<td><center><%=current.getCategory()%></center></td>
											
											<%int status = current.getVerification_status();
											String status_msg = "";
											switch (status) {
												case 0 :
													status_msg = "reported";
													break;
												case 1 :
													status_msg = "data entered";
													break;
												case 2 :
													status_msg = "data approved";
													break;
												case 3 :
													status_msg = "fee paid";
													break;
												case 4 :
													status_msg = "fee verified";
													break;
												case 5 :
													status_msg = "registered";
													break;
												case 6 :
													status_msg = "Pass Out";
													break;
												case 7 :
													status_msg = "Course withdraw";
													break;
												case 8 :
													status_msg = "Terminate";
													break;
											}
											out.println("<td><center>"+status_msg+"</center></td>");
%>
											
										</tr>
										<%
                }
				%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->

<script type="text/javascript">
window.print();
</script>
</body>
</html>